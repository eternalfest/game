import chai from "chai";
import { URL } from "url";
import { getGamePath, getGameUri } from "../lib";

describe("getGamePath", () => {
  it("returns a string", () => {
    const actual: string = getGamePath();
    chai.assert.isString(actual);
  });
});

describe("getGameUri", () => {
  it("returns a file URI", () => {
    const actual: URL = getGameUri();
    chai.assert.instanceOf(actual, URL);
    chai.assert.strictEqual(actual.protocol, "file:");
  });
});
