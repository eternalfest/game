import sysPath from "path";
import url from "url";
import meta from "./meta.js";

export function getGamePath(): string {
  return sysPath.resolve(meta.dirname, "game.swf");
}

export function getGameUri(): url.URL {
  return url.pathToFileURL(getGamePath());
}
