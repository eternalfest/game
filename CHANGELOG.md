# 1.1.0 (2019-12-28)

- **[Feature]** Add `getGameUri`.
- **[Fix]** Update dependencies.

# 1.0.0 (2019-08-23)

- **[Feature]** First release.
